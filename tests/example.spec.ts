import { test, expect } from '@playwright/test';

// test('has title', async ({ page }) => {
//   await page.goto('https://internal.citigov.id/signin');

//   // Expect a title "to contain" a substring.
//   await expect(page).toHaveTitle(/Selamat Datang!/);
// });

// test('get started link', async ({ page }) => {
//   await page.goto('https://playwright.dev/');

//   // Click the get started link.
//   await page.getByRole('link', { name: 'Get started' }).click();

//   // Expects page to have a heading with the name of Installation.
//   await expect(page.getByRole('heading', { name: 'Installation' })).toBeVisible();
// });

test('has title', async ({ page }) => {
  await page.goto('https://internal.citigov.id/signin');

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/CARTENZ GROUP | Signin/);
});

 // /img/cartenz/bg-signin.png

test('has welcome text', async({ page}) => {
  await page.goto('https://internal.citigov.id/signin');
  
  await expect(page.getByText('Selamat Datang!')).toBeVisible();
})

