import { test, chromium,  expect } from "@playwright/test"

test("Register New", async() => {
    const browser = await chromium.launch({
        headless: false
    });
    const context = await browser.newContext();
    const page = await context.newPage();
    const locatorButton = await 

    await page.goto("https://internal.citigov.id/signin")

    await page.click("text=Daftar Sekarang")
    await page.fill("input[name='fullName']", "Nama Inna")
    await page.fill("input[name='password']", "Test123!")
    await page.fill("input[name='email']", "inna.chica@gmail.com")
    await page.fill("input[name='confirmPassword']", "Test123!")
    await page.fill("input[name='telp']", "08129090112")
    await page.getByRole('button', {name: 'Daftar'}).click();
    // await page.waitForTimeout(5000)
})

test('Click button via OTP', async ({ page }) => {
    await page.goto('https://internal.citigov.id/signin');
    await page.getByRole('button', { name: 'OTP via Email' }).click();
  });



test('Login', async ({ page }) => {
  await page.goto('https://internal.citigov.id/signin');
  await page.getByPlaceholder('Masukkan email').click();
  await page.getByPlaceholder('Masukkan email').fill('inna.chica@gmail.com');
  await page.getByRole('button', { name: 'Kata sandi' }).click();
  await page.getByPlaceholder('Masukan kata sandi').click();
  await page.getByPlaceholder('Masukan kata sandi').fill('Test123!');
  await page.getByRole('button', { name: 'Masuk' }).click();
});


test('Request Pelayanan Not Full', async ({ page }) => {
    await page.goto('https://internal.citigov.id/citizen/pelayanan-create');
    await page.getByRole('link', { name: 'Login' }).click();
    await page.getByPlaceholder('Masukkan email').click();
    await page.getByPlaceholder('Masukkan email').fill('inna.chica@gmail.com');
    await page.getByRole('button', { name: 'Kata sandi' }).click();
    await page.getByPlaceholder('Masukan kata sandi').click();
    await page.getByPlaceholder('Masukan kata sandi').fill('Test123!');
    await page.getByRole('button', { name: 'Masuk' }).click();
    await page.getByRole('button', { name: 'Tutup' }).click();
    await page.getByRole('button', { name: 'Kembali ke login' }).click();
    await page.getByRole('button', { name: 'OTP via Email' }).click();
    await page.getByRole('button', { name: 'Tutup' }).click();
    await page.getByPlaceholder('Masukkan email').click();
    await page.getByPlaceholder('Masukkan email').fill('inna.chica@gmail.com');
    await page.getByRole('button', { name: 'OTP via Email' }).click();
    await page.getByRole('button', { name: 'Oke' }).click();
    await page.locator('input[name="otp1"]').click();
    await page.locator('input[name="otp1"]').fill('0');
    await page.locator('input[name="otp2"]').fill('8');
    await page.locator('input[name="otp3"]').fill('8');
    await page.locator('input[name="otp4"]').fill('5');
    await page.getByRole('link', { name: 'Pelayanan', exact: true }).click();
    await page.getByText('Ajukan').click();
    await page.getByText('Ajukan').click();
    await page.getByText('Pilih Jenis Pemohon').click();
    await page.getByText('Pemilik', { exact: true }).click();
    await page.getByText('WNI').click();
    await page.locator('div').filter({ hasText: /^Swasta$/ }).first().click();
    await page.locator('div').filter({ hasText: /^Insidentil$/ }).nth(1).click();
    await page.getByPlaceholder('Masukkan No. KTP').click();
    await page.getByPlaceholder('Masukkan No. KTP').fill('313131313131');
    await page.getByPlaceholder('Masukkan Nama Lengkap').click();
    await page.getByPlaceholder('Masukkan Nama Lengkap').fill('Inna Norman');
    await page.getByPlaceholder('Masukkan Tempat Lahir').click();
    await page.getByPlaceholder('Masukkan Tempat Lahir').fill('Cirebon');
    await page.getByPlaceholder('Masukkan Nama Lengkap').click();
    await page.getByPlaceholder('Masukkan Nama Lengkap').fill('Inna Normaningsih');
    await page.getByPlaceholder('Masukkan Tanggal').click();
    await page.getByRole('cell', { name: '1995' }).click();
    await page.getByRole('cell', { name: 'Agt' }).click();
    await page.getByRole('cell', { name: '08' }).first().click();
    await page.getByPlaceholder('Masukkan Alamat Pemohon').click();
    await page.getByPlaceholder('Masukkan Alamat Pemohon').fill('jl cempaka');
    await page.getByPlaceholder('Masukkan Negara Pemohon').click();
    await page.getByText('Pilih Provinsi Provinsi', { exact: true }).click();
    await page.getByText('JAKARTA', { exact: true }).click();
    await page.getByText('Pilih Provinsi Kabupaten/ Kota Provinsi').click();
    await page.locator('#react-select-25-option-20').click();
    await page.getByText('Pilih Kabupaten/ Kota Kabupaten/ Kota Provinsi').click();
    await page.getByText('KOTA JAKARTA TIMUR', { exact: true }).click();
    await page.getByText('Pilih Provinsi Kecamatan', { exact: true }).click();
    await page.locator('#react-select-27-option-20').click();
    await page.getByText('Pilih Kabupaten/ Kota Kecamatan', { exact: true }).click();
    await page.locator('#react-select-28-option-4').click();
    await page.getByText('Pilih Kecamatan Kecamatan', { exact: true }).click();
    await page.getByText('CIRACAS', { exact: true }).click();
    await page.getByText('Pilih Provinsi Kelurahan', { exact: true }).click();
    await page.locator('#react-select-30-option-20').click();
    await page.getByText('Pilih Kabupaten/ Kota Kelurahan', { exact: true }).click();
    await page.locator('#react-select-31-option-4').click();
    await page.getByText('Pilih Kecamatan Kelurahan', { exact: true }).click();
    await page.locator('#react-select-32-option-2').click();
    await page.getByText('Pilih Kelurahan Kelurahan', { exact: true }).click();
    await page.getByText('KELAPA DUA WETAN', { exact: true }).click();
    await page.getByPlaceholder('Masukkan No Telepon Pemohon').click();
    await page.getByPlaceholder('Masukkan No Telepon Pemohon').fill('085694969854');
    await page.getByPlaceholder('Masukkan No. HP Pemohon').click();
    await page.getByPlaceholder('Masukkan No. HP Pemohon').fill('085694969854');
    await page.getByPlaceholder('Masukkan Negara Perusahaan').click();
    await page.getByPlaceholder('Masukkan Negara Perusahaan').fill('Indonesia');
    await page.getByText('Pilih Provinsi Provinsi').click();
    await page.locator('#react-select-34-option-20').click();
    await page.getByText('Pilih Provinsi Kabupaten/').click();
    await page.locator('#react-select-35-option-20').click();
    await page.getByText('Pilih Kabupaten/ Kota Kabupaten/ Kota Wilayah Perusahaan').click();
    await page.locator('#react-select-36-option-4').click();
    await page.getByText('Pilih Provinsi Kecamatan').click();
    await page.locator('#react-select-37-option-20').click();
    await page.getByText('Pilih Kabupaten/ Kota Kecamatan Wilayah Perusahaan').click();
    await page.locator('#react-select-38-option-4').click();
    await page.getByText('Pilih Kecamatan Kecamatan').click();
    await page.locator('#react-select-39-option-2').click();
    await page.getByText('Pilih Provinsi Kelurahan').click();
    await page.locator('#react-select-40-option-20').click();
    await page.getByText('Pilih Kabupaten/ Kota').click();
    await page.locator('#react-select-41-option-4').click();
    await page.getByText('Pilih Kecamatan Kelurahan').click();
    await page.locator('#react-select-42-option-2').click();
    await page.getByText('Pilih Kelurahan Kelurahan').click();
    await page.locator('#react-select-43-option-2').click();
    await page.getByPlaceholder('Masukkan Judul Reklame').click();
    await page.getByPlaceholder('Masukkan Judul Reklame').fill('Promosi');
    await page.getByPlaceholder('Masukkan Link Spreadsheet').click();
    await page.getByPlaceholder('Masukkan Link Spreadsheet').fill('https://docs.google.com/spreadsheets/d/1LygGhIvqb9LIBHv4VjlB-oOVEh5jUi96/edit?usp=sharing&ouid=11252');
    await page.getByPlaceholder('Masukkan Link Open Repository').click();
    await page.getByPlaceholder('Masukkan Link Open Repository').fill('https://docs.google.com/spreadsheets/d/1LygGhIvqb9LIBHv4VjlB-oOVEh5jUi96/edit?usp=sharing&ouid=11252');
    await page.getByPlaceholder('Masukkan Link Video Test').click();
    await page.getByPlaceholder('Masukkan Link Video Test').fill('https://docs.google.com/spreadsheets/d/1LygGhIvqb9LIBHv4VjlB-oOVEh5jUi96/edit?usp=sharing&ouid=11252');
    await page.locator('div').filter({ hasText: /^File Dokumen A \*Lampirkan file disini$/ }).locator('span').nth(1).click();
    await page.locator('body').setInputFiles('inna-normaningsih-cv.pdf');
    await page.locator('div').filter({ hasText: /^File Dokumen B Lampirkan file disini$/ }).locator('span').nth(1).click();
    await page.locator('body').setInputFiles('inna-normaningsih-cv.pdf');
    await page.getByRole('button', { name: 'Kirim' }).click();
  });

  test('Request Pelayanan Sucess Regist', async ({ page }) => {
    await page.goto('https://internal.citigov.id/citizen/pelayanan-create');
    await page.getByRole('link', { name: 'Login' }).click();
    await page.getByPlaceholder('Masukkan email').click();
    await page.getByPlaceholder('Masukkan email').fill('inna.chica@gmail.com');
    await page.getByRole('button', { name: 'Kata sandi' }).click();
    await page.getByPlaceholder('Masukan kata sandi').click();
    await page.getByPlaceholder('Masukan kata sandi').fill('Test123!');
    await page.getByRole('button', { name: 'Masuk' }).click();
    await page.getByRole('button', { name: 'Tutup' }).click();
    await page.getByRole('button', { name: 'Kembali ke login' }).click();
    await page.getByRole('button', { name: 'OTP via Email' }).click();
    await page.getByRole('button', { name: 'Tutup' }).click();
    await page.getByPlaceholder('Masukkan email').click();
    await page.getByPlaceholder('Masukkan email').fill('inna.chica@gmail.com');
    await page.getByRole('button', { name: 'OTP via Email' }).click();
    await page.getByRole('button', { name: 'Oke' }).click();
    await page.locator('input[name="otp1"]').click();
    await page.locator('input[name="otp1"]').fill('0');
    await page.locator('input[name="otp2"]').fill('8');
    await page.locator('input[name="otp3"]').fill('8');
    await page.locator('input[name="otp4"]').fill('5');
    await page.getByRole('link', { name: 'Pelayanan', exact: true }).click();
    await page.getByText('Ajukan').click();
    await page.getByText('Ajukan').click();
    await page.getByText('Pilih Jenis Pemohon').click();
    await page.getByText('Pemilik', { exact: true }).click();
    await page.getByText('WNI').click();
    await page.locator('div').filter({ hasText: /^Swasta$/ }).first().click();
    await page.locator('div').filter({ hasText: /^Insidentil$/ }).nth(1).click();
    await page.getByPlaceholder('Masukkan No. KTP').click();
    await page.getByPlaceholder('Masukkan No. KTP').fill('313131313131');
    await page.getByPlaceholder('Masukkan Nama Lengkap').click();
    await page.getByPlaceholder('Masukkan Nama Lengkap').fill('Inna Norman');
    await page.getByPlaceholder('Masukkan Tempat Lahir').click();
    await page.getByPlaceholder('Masukkan Tempat Lahir').fill('Cirebon');
    await page.getByPlaceholder('Masukkan Nama Lengkap').click();
    await page.getByPlaceholder('Masukkan Nama Lengkap').fill('Inna Normaningsih');
    await page.getByPlaceholder('Masukkan Tanggal').click();
    await page.getByRole('cell', { name: '1995' }).click();
    await page.getByRole('cell', { name: 'Agt' }).click();
    await page.getByRole('cell', { name: '08' }).first().click();
    await page.getByPlaceholder('Masukkan Alamat Pemohon').click();
    await page.getByPlaceholder('Masukkan Alamat Pemohon').fill('jl cempaka');
    await page.getByPlaceholder('Masukkan Negara Pemohon').click();
    await page.getByText('Pilih Provinsi Provinsi', { exact: true }).click();
    await page.getByText('JAKARTA', { exact: true }).click();
    await page.getByText('Pilih Provinsi Kabupaten/ Kota Provinsi').click();
    await page.locator('#react-select-25-option-20').click();
    await page.getByText('Pilih Kabupaten/ Kota Kabupaten/ Kota Provinsi').click();
    await page.getByText('KOTA JAKARTA TIMUR', { exact: true }).click();
    await page.getByText('Pilih Provinsi Kecamatan', { exact: true }).click();
    await page.locator('#react-select-27-option-20').click();
    await page.getByText('Pilih Kabupaten/ Kota Kecamatan', { exact: true }).click();
    await page.locator('#react-select-28-option-4').click();
    await page.getByText('Pilih Kecamatan Kecamatan', { exact: true }).click();
    await page.getByText('CIRACAS', { exact: true }).click();
    await page.getByText('Pilih Provinsi Kelurahan', { exact: true }).click();
    await page.locator('#react-select-30-option-20').click();
    await page.getByText('Pilih Kabupaten/ Kota Kelurahan', { exact: true }).click();
    await page.locator('#react-select-31-option-4').click();
    await page.getByText('Pilih Kecamatan Kelurahan', { exact: true }).click();
    await page.locator('#react-select-32-option-2').click();
    await page.getByText('Pilih Kelurahan Kelurahan', { exact: true }).click();
    await page.getByText('KELAPA DUA WETAN', { exact: true }).click();
    await page.getByPlaceholder('Masukkan No Telepon Pemohon').click();
    await page.getByPlaceholder('Masukkan No Telepon Pemohon').fill('085694969854');
    await page.getByPlaceholder('Masukkan No. HP Pemohon').click();
    await page.getByPlaceholder('Masukkan No. HP Pemohon').fill('085694969854');
    await page.getByPlaceholder('Masukkan Negara Perusahaan').click();
    await page.getByPlaceholder('Masukkan Negara Perusahaan').fill('Indonesia');
    await page.getByText('Pilih Provinsi Provinsi').click();
    await page.locator('#react-select-34-option-20').click();
    await page.getByText('Pilih Provinsi Kabupaten/').click();
    await page.locator('#react-select-35-option-20').click();
    await page.getByText('Pilih Kabupaten/ Kota Kabupaten/ Kota Wilayah Perusahaan').click();
    await page.locator('#react-select-36-option-4').click();
    await page.getByText('Pilih Provinsi Kecamatan').click();
    await page.locator('#react-select-37-option-20').click();
    await page.getByText('Pilih Kabupaten/ Kota Kecamatan Wilayah Perusahaan').click();
    await page.locator('#react-select-38-option-4').click();
    await page.getByText('Pilih Kecamatan Kecamatan').click();
    await page.locator('#react-select-39-option-2').click();
    await page.getByText('Pilih Provinsi Kelurahan').click();
    await page.locator('#react-select-40-option-20').click();
    await page.getByText('Pilih Kabupaten/ Kota').click();
    await page.locator('#react-select-41-option-4').click();
    await page.getByText('Pilih Kecamatan Kelurahan').click();
    await page.locator('#react-select-42-option-2').click();
    await page.getByText('Pilih Kelurahan Kelurahan').click();
    await page.locator('#react-select-43-option-2').click();
    await page.getByPlaceholder('Masukkan Judul Reklame').click();
    await page.getByPlaceholder('Masukkan Judul Reklame').fill('Promosi');
    await page.getByPlaceholder('Masukkan Jumlah').click();
    await page.getByPlaceholder('Masukkan Jumlah').fill('2');
    await page.getByPlaceholder('Masukkan Link Spreadsheet').click();
    await page.getByPlaceholder('Masukkan Link Spreadsheet').fill('https://docs.google.com/spreadsheets/d/1wfXzOYcEhhTmKwQ2MEn0-UaCaoaFheMg/edit?usp=sharing&ouid=106055049865003744447&rtpof=true&sd=true');
    await page.getByPlaceholder('Masukkan Link Open Repository').click();
    await page.getByPlaceholder('Masukkan Link Open Repository').fill('https://gitlab.com/innanorman/test-cartenz-playwright');
    await page.getByPlaceholder('Masukkan Link Video Test').click();
    await page.getByPlaceholder('Masukkan Link Video Test').fill('https://drive.google.com/file/d/1auBLiO-3GK0M3EJkxnLz5N0kGV513yoL/view?usp=sharing');
    await page.locator('div').filter({ hasText: /^File Dokumen A \*Lampirkan file disini$/ }).locator('span').nth(1).click();
    await page.locator('body').setInputFiles('inna-normaningsih-cv.pdf');
    await page.locator('div').filter({ hasText: /^File Dokumen B Lampirkan file disini$/ }).locator('span').nth(1).click();
    await page.locator('body').setInputFiles('inna-normaningsih-cv.pdf');
    await page.getByRole('button', { name: 'Kirim' }).click();
  });