import { PlaywrightTestConfig} from '@playwright/test';

const config: PlaywrightTestConfig = {
  testMatch: ["tests/login.spec.ts"],
  use: {
    headless: false,
    screenshot: "only-on-failure",
    video: "retain-on-failure"
  }, 
  reporter: [["dot"], ["json", {
    outputFile: "jsonReports/jsonReport.json"
  }],
  ["html", {
    open: "never"
  }]
]
}

export default config